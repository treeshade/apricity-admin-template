import { getCurrentInstance } from "vue";

const timeoutManager = {} as any;

export const isNumber = (value: never): boolean => {
  return !isNaN(parseFloat(value)) && isFinite(value);
};

export const isMobilePhone = (): boolean => {
  return document.body.clientWidth <= 768;
};

/**
 * @description 判断是否是移动端设备
 * @public
 * @returns Boolean
 */
export const isMobile = (): boolean => {
  return !!window.top.navigator.userAgent.match(/(iPhone|iPod|Android|ios)/i);
};

/**
 * @description 高频率执行延时操作时，只执行最后一次动作
 * @public
 * @param {String} flag 唯一标识一次延时，例如：searchInputTimeout
 * @param {Function} callback 执行方法
 * @param {Number} timeout
 * @param {TouchEvent} env
 */
export const finalDelay = (
  flag: string,
  callback: (env: Event) => void,
  timeout: number,
  env: Event
): void => {
  if (timeoutManager[flag]) {
    clearTimeout(timeoutManager[flag]);
  }
  timeoutManager[flag] = setTimeout(() => {
    callback(env);
    delete timeoutManager[flag];
  }, timeout);
};

/**
 * @description 模拟事件
 * @public
 * @param dom 需要触发的dom,默认为全局document
 * @param event 需要触发的事件名
 */
export const simulatedEvent = (dom: HTMLDocument, event: string): void => {
  !(dom instanceof HTMLElement) && (dom = document);
  try {
    dom.dispatchEvent(new Event(event));
  } catch {
    const evt = document.createEvent("HTMLEvents");
    evt.initEvent(event, false, true);
    dom.dispatchEvent(evt);
  }
};

export function useGlobalConfig() {
  const vm: any = getCurrentInstance();
  if ("$APRICITY" in vm.proxy) {
    return vm.proxy.$APRICITY;
  }
  return {};
}
