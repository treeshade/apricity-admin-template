import axios from "axios";
import {useStore} from "@/store";

// 请求超时时间，10s
const requestTimeOut = 10 * 1000;
const success = 200;

const http = axios.create({
  baseURL: "",
  timeout: requestTimeOut,
  validateStatus(status) {
    return status === success;
  },
});

http.interceptors.request.use(
  (config) => {
    const _config = config;
    const store = useStore()
    config.headers["Authorization"] = "bearer" + store.state.user.token;
    return _config;
  },
  (error) => {
    console.error(error);
    return Promise.reject(error);
  }
);

http.interceptors.response.use(
  (config) => {
    return config;
  },
  (error) => {
    console.error(error);
  }
);

export default http;
