import http from "@/utils/https";

export const login = (user: any) => {
  return http.post("/vue-element-admin/user/login", user);
};

export const userInfo = (token: string) => {
  return http.get("/vue-element-admin/user/info.*", {
    params: { token: token },
  });
};
