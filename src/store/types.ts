export interface IAppState {
  logo: string;
}

export interface ISettingsState {
  logo: string;
}

export interface ISwitchTabsState {
  logo: string;
}

export interface RootState {
  app: IAppState;
  settings: ISettingsState;
  switchTabs: ISwitchTabsState;
}
