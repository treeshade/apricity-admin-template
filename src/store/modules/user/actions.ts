import { ActionTree, ActionContext } from "vuex";
import { RootState } from "@/store";
import { useStore } from "vuex";
import { state, UserState } from "./state";
import { Mutations } from "./mutations";
import { UserMutationTypes } from "./mutation-types";
import { UserActionTypes } from "./action-types";
import { login, userInfo } from "@/api/user.ts";
import { removeToken, setToken, getToken } from "@/utils/cookies";
// import { PermissionActionType } from "../permission/action-types";
// import router, { resetRouter } from "@/router";
import router from "@/router";
import { RouteRecordRaw } from "vue-router";

type AugmentedActionContext = {
  commit<K extends keyof Mutations>(
    key: K,
    payload: Parameters<Mutations[K]>[1]
  ): ReturnType<Mutations[K]>;
} & Omit<ActionContext<UserState, RootState>, "commit">;

export interface Actions {
  [UserActionTypes.ACTION_LOGIN](
    { commit }: AugmentedActionContext,
    userInfo: { username: string; password: string }
  ): void;
  [UserActionTypes.ACTION_RESET_TOKEN]({
    commit,
  }: AugmentedActionContext): void;
  [UserActionTypes.ACTION_GET_USER_INFO]({
    commit,
  }: AugmentedActionContext): void;
  [UserActionTypes.ACTION_CHANGE_ROLES](
    { commit }: AugmentedActionContext,
    role: string
  ): void;
  [UserActionTypes.ACTION_LOGIN_OUT]({ commit }: AugmentedActionContext): void;
}

export const actions: ActionTree<UserState, RootState> & Actions = {
  async [UserActionTypes.ACTION_LOGIN](
    { commit }: AugmentedActionContext,
    userInfo: { username: string; password: string }
  ) {
    let { username, password } = userInfo;
    username = username.trim();
    password = password.trim();
    await login({ username, password })
      .then(async (res: any) => {
        if (res?.data.code === 20000 && res.data.data.token) {
          setToken(res.data.data.token);
          commit(UserMutationTypes.SET_TOKEN, res.data.data.token);
        }
      })
      .catch((err) => {
        console.log(err);
      });
  },

  [UserActionTypes.ACTION_RESET_TOKEN]({ commit }: AugmentedActionContext) {
    removeToken();
    commit(UserMutationTypes.SET_TOKEN, "");
    commit(UserMutationTypes.SET_ROLES, []);
  },

  async [UserActionTypes.ACTION_GET_USER_INFO]({
    commit,
  }: AugmentedActionContext) {
    if (state.token === "") {
      throw Error("token is undefined!");
    }
    await userInfo(state.token).then((res: any) => {
      console.log('userInfo res')
      console.log(res)
      if (res?.data.code === 20000) {
        commit(UserMutationTypes.SET_ROLES, res.data.data.roles);
        commit(UserMutationTypes.SET_NAME, res.data.data.name);
        commit(UserMutationTypes.SET_AVATAR, res.data.data.avatar);
        commit(UserMutationTypes.SET_INTRODUCTION, res.data.data.introduction);
        commit(UserMutationTypes.SET_EMAIL, res.data.data.email);
        return res;
      } else {
        throw Error("Verification failed, please Login again.");
      }
    });
  },

  async [UserActionTypes.ACTION_CHANGE_ROLES](
    { commit }: AugmentedActionContext,
    role: string
  ) {
    // const token = role + "-token";
    // const store = useStore();
    // commit(UserMutationTypes.SET_TOKEN, token);
    // setToken(token);
    // await store.dispatch(UserActionTypes.ACTION_GET_USER_INFO, undefined);
    // store.dispatch(PermissionActionType.ACTION_SET_ROUTES, state.roles);
    // store.state.permission.dynamicRoutes.forEach((item: RouteRecordRaw) => {
    //   router.addRoute(item);
    // });
  },

  [UserActionTypes.ACTION_LOGIN_OUT]({ commit }: AugmentedActionContext) {
    console.log(commit);
    removeToken();
    commit(UserMutationTypes.SET_TOKEN, "");
    commit(UserMutationTypes.SET_ROLES, []);
    // resetRouter();
  },
};
