import { createStore } from "vuex";

import {
  store as tagViews,
  TagsStore,
  TagsViewState,
} from "@/store/modules/tagsview";
import { store as user, UserStore, UserState } from "@/store/modules/user";

export type Store = UserStore<Pick<RootState, "user">> &
    TagsStore<Pick<RootState, "tagViews">>;

export interface RootState {
  tagViews: TagsViewState;
  user: UserState;
}

export const store = createStore({
  modules: {
    user,
    tagViews,
  },
});

export function useStore(): Store {
  return store as Store;
}
