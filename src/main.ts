import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import {store} from "./store";
import "normalize.css";
import Icon from "@/components/icon";
import SvgIcon from "@/components/svg-icon";
import Scroll from "@/components/scroll";
import { Form, Input, Button, Menu, Dropdown } from "ant-design-vue";
import "./permission";

import mockXHR from "../mock/index.js";
mockXHR();

createApp(App)
  .use(Form)
  .use(Input)
  .use(Button)
  .use(Menu)
  .use(Dropdown)
  .use(SvgIcon)
  .use(Scroll)
  .use(Icon)
  .use(store)
  .use(router)
  .mount("#app");
