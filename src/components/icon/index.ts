import Icon from "./icon.vue";
import { withInstall } from "@/utils";

export default withInstall(Icon);
