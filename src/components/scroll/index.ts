import Scroll from "./scroll.vue";
import { withInstall } from "@/utils";

export default withInstall(Scroll);
