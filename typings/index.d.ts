declare type Nullable<T> = T | null;
declare module "*.js";
declare type ComponentSize = "large" | "medium" | "small" | "mini";
